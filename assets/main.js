// Sometimes googles misses onload for recaptcha, onload will handle those cases.
window.onload = function() {
    if($('#recaptcha iframe').length == 0) {
        recaptchaCallback();
    }
}
window.addEventListener('orientationchange', function() {
    var captchawidth = $('#recaptcha').width()/304;
    // Making captch width equal to captcha wrapper.
    $('#recaptcha iframe').css('transform', 'scale('+captchawidth+', 1)');
});

$(function () {
    // Removing preloader on document ready
    $("#preloader").fadeOut("slow", function () {
        $(this).remove();
    });
    // Nano scroll visibility fixes.
    $(".nano").nanoScroller({alwaysVisible: true });

    $("a[data-toggle='tab']")
        .on("shown.bs.tab", function () {
            $(".nano").nanoScroller({ alwaysVisible: true });
        });

    //Start Animating parallax initiation.

    window.total_num_pages = $("div.parallax-page").length;
    this.ANIM = new HORILLAX.ANIM(function onAnimEnd() {
        var left = parseInt(this[0].style.left, 10);
        if (left !== 0) {
            this.trigger("parallax:offscreen");
        } else {
            this.trigger("parallax:onscreen");
        }
    });
    HORILLAX.ANIM.instance.move(pagemove);

    // Handle auto play & stop for videos in modals
    $(".modal")
        .on("show.bs.modal", function () {
            var vids = $(this).find("video");
            if (vids.length) {
                vids[0].play();
            }
        })
        .on("hide.bs.modal", function () {
            var vids = $(this).find("video");
            if (vids.length) {
                vids[0].pause(); // no stop method available, only pause
                vids[0].currentTime = 0; // reset to start
            }
        });

    // Ensure the correct tab is highlighted when opening a tab via the feature Links
    // by retrigger the click on the corresponding tab element
    $(".tab_trigger").on("click", function (evt) {
        var href = evt.currentTarget.id.split("_")[0];
        var $targetTab = $("a[href='#" + href + "']");
        $targetTab.click();
    });

    // Ensure that each page's tabs are reset to the intro tab when they go offscreen
    $(".nav-tabs").each(function (idx, el) {
        var $tabs = $(el);
        $tabs.parents(".parallax-page").on("parallax:offscreen", function (evt) {
            $("a[data-toggle='tab']:first", $tabs).click();
        });
    });

    // WHH feed horizontal on start fix.

    $('#aboutus-page .social-feed-ji').one('click', function () {
        $('ul.juicer-feed').hide().css('visibility', 'hidden').css('opacity', 0);
        setTimeout(function () {
            $('ul.juicer-feed').trigger('resize');
        }, 200);
        setTimeout(function () {
            $('ul.juicer-feed').fadeIn("slow").css('visibility', 'visible').css('opacity', 1);
        }, 400);
    });

    // make it easier to close modal on tablet
    $(".modal-header").click(function () {
        $(".modal").modal("hide");
    });

    function stringEndsWith(string_to_test, suffix) {
        if (string_to_test === null || suffix === null) {
            return false;
        }
        return string_to_test.indexOf(suffix, string_to_test.length - suffix.length) !== -1;
    }

    var current_url = window.location.pathname.toLowerCase();
    if (stringEndsWith(current_url, 'thevillages') || stringEndsWith(current_url, 'thevillages/')) {
        setTimeout(function () {
            $('#thevillagestargetnav a').trigger('click');
        }, 1000);
    }

    // Validation for contact form.

    $('#contactform')
        .bootstrapValidator({
            message: 'This value is not valid',
//            feedbackIcons: {
//                valid: 'glyphicon glyphicon-ok',
//                invalid: 'glyphicon glyphicon-remove',
//                validating: 'glyphicon glyphicon-refresh'
//            },
            fields: {
                fname: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter your first name (at least 3 characters)'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'The first name must be more than 3 and less than 30 characters long'
                        }
                    }
                },
                captcha: {
                    validators: {
                        notEmpty: {
                            message: 'Please complete the captcha'
                        }
                    }
                },
                lname: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter your last name (at least 3 characters)'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'The last name must be more than 3 and less than 30 characters long'
                        }
                    }
                },
                company: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter your company name (at least 2 characters)'
                        },
                        stringLength: {
                            min: 2,
                            max: 30,
                            message: 'The company name must be more than 2 and less than 30 characters long'
                        }
                    }
                },
                contactno: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter your phone no (at least 8 characters)'
                        },
                        stringLength: {
                            min: 8,
                            max: 15,
                            message: 'The phone no must be more than 8 and less than 15 characters long'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required and can\'t be empty'
                        },
                        regexp: {
                            regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                            message: 'Please enter a valid email address'
                        }
                    }
                },
                bmoc: {
                    validators: {
                        notEmpty: {
                            message: 'How would you like to be contacted?'
                        }
                    }
                },
                bttc: {
                    validators: {
                        notEmpty: {
                            message: 'When is the best time to contact you?'
                        }
                    }
                },
                inquirytype: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter the type of inquiry you have'
                        }
                    }
                },
                'description': {
                    validators: {
                        notEmpty: {
                            message: 'Please enter your message (at least 10 characters)'
                        },
                        stringLength: {
                            min: 10,
                            max: 250,
                            message: 'The message must be more than 10 and less than 250 characters long'
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function (e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');
            //console.log("Start Loader");
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function () {

            })
            .fail(function() {
                $('#contactform').html("<h3>OOPS some error happened, please reload the page and try again!</h3>");
            })
            .done(function (data) {
                 $('#contactform').html("<h3>" + data + "</h3>");
            });
        });
    $("[data-bv-validator='emailAddress']").remove();

    $("#business-set-up a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate    //
        // $('html, body').animate({
        //         scrollTop: $(hash).offset().top
        //     }, 100, function(){
        //         // when done, add hash to url
        //         // (default click behaviour)
        //         window.location.hash = hash;
        // 	}
        // );

    });

    // Recaptcha scaling
    $('.g-recaptcha iframe').css('transform', 'scale(1.1, 1)');
});
