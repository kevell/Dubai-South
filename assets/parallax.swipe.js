(function ($) {
    "use strict";

/*=================================================
	Scroll Page on Swipe
==================================================*/

    jQuery("body").swipe({
        swipeLeft: function (event, direction, distance, duration, fingerCount) {
            if (pagemove < (total_num_pages - 1)) {
                pagemove++;
                HORILLAX.ANIM.instance.move(pagemove);
            }
        },
        swipeRight: function (event, direction, distance, duration, fingerCount) {
            if (pagemove > 0) {
                pagemove--;
                HORILLAX.ANIM.instance.move(pagemove);
            }
        }
    });



})(jQuery);
